-- This program controls the fence of the staple
-- It is meant to be used with an advanced screen as a control interface
-- but can easily be adapted to use a redstone signal instead.

state = false
sand = "back"
top = "bottom"
pulse = top
bottom = "left"
monitor = peripheral.wrap("monitor_2")
function ticktop()
    rs.setOutput(pulse, true)
    os.sleep(0.2)
    rs.setOutput(pulse, false)
end

function paintQuarter(number, color)
    local l, h = monitor.getSize()
    local quarters = {
        [1] = {0;0};
        [2] = {l/2;0};
        [3] = {0;h/2};
        [4] = {l/2; h/2}
        }
    local t_colors = {
        [colors.green] = "d";
        [colors.red] = "e"
        }
    local chr = " "
    for i=1+quarters[number][1], l/2+quarters[number][1] do
        for j=1+quarters[number][2], h/2+quarters[number][2] do
            if i == 1+quarters[number][1] or i ==  l/2+quarters[number][1] or j == 1+quarters[number][2] or j == h/2+quarters[number][2] then
                chr = "+"
            else
                chr = " "
            end
            monitor.setCursorPos(i,j)
            monitor.blit(chr, "0", t_colors[color])
        end
    end
    monitor.setCursorPos(math.ceil(l/4) + quarters[number][1], math.ceil(h/4) + quarters[number][2])
    monitor.blit(tostring(number), "0", t_colors[color])
end

function waitForClick(state, number)
    local l, h = monitor.getSize()
    local color
    if state then
        color = colors.green
    else
        color = colors.red
    end
    paintQuarter(number, color)
    local event, button, x, y = os.pullEvent("monitor_touch")
    
    if number == 1 and x <= l/2 and y <= h/2 then
        return true
    elseif number == 2 and x > l/2 and y <= h/2 then
        return true
    elseif number == 3 and x <= l/2 and y > h/2 then
        return true
    elseif number == 4 and x > l/2 and y > h/2 then
        return true
    else
        return false
    end
end


function main(number)
    rs.setOutput(pulse, false)
    rs.setOutput(bottom, false)
    os.sleep(0.5)
    rs.setOutput(sand, true)
    os.sleep(0.5)
    rs.setOutput(top, true)
    while 1 do
        if waitForClick(state, number) then
            state = not state
            if state then -- Open the gate
                rs.setOutput(top, false)
                os.sleep(0.2)
                rs.setOutput(sand, false)
                os.sleep(0.2)
                rs.setOutput(bottom, true)
                os.sleep(0.2)
                ticktop()
                os.sleep(0.2)
                rs.setOutput(bottom, false)
                os.sleep(0.2)
                rs.setOutput(sand, true)
                os.sleep(0.1)
            else -- Close the gate
                rs.setOutput(top, true)
            end
        end
    end
end

main(1)
