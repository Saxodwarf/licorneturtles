local allowed = {["minecraft:leaves2"] = true;
                 ["minecraft:log2"] = true}


function digUp()
    local item = turtle.inspectUp()
    if item and allowed[item.name] then
        turtle.digUp()
    else 
        return false
    end
    return true
end
function digDown()
    local item = turtle.inspectDown()
    if item and allowed[item.name] then
        turtle.digDown()
    else 
        return false
    end
    return true
end
function dig()
    local item = turtle.inspect()
    if item and allowed[item.name] then
        turtle.dig()
    else 
        return false
    end
    return true
end



function waitForOtherRobot()
    print("Waiting for other robot")
    local id, message, prot = rednet.receive("tree")
    return message == "done"
end

function refill(nb)
    turtle.turnRight()
    turtle.suck(nb)
    turtle.turnLeft()
--     local item
--     for i in 3,16 do
--         turtle.select(i)
--         item = turtle.getItemDetail()
--         if item then
--             if item.name == 'minecraft:dye' then
--                 turtle.transferTo(2)
--             end
--         end
--     end
end

function check_bone_meal()
    local item
    turtle.select(2)
    item = turtle.getItemDetail()
    if item then
        if item.name == 'minecraft:dye' then
            if item.count < 25 then
                refill(64-item.count)
            end
        end
    else
        refill()
    end
end

function refuel(limit)
    if turtle.getFuelLevel()< limit then
        turtle.turnRight()
        turtle.turnRight()
        
        turtle.select(16)
        turtle.suck()
        turtle.refuel()
        turtle.drop()
        turtle.turnRight()
        turtle.turnRight()
    end
end

function emptyLeftoverChest()
    -- Sort chest and refill sapling
    turtle.turnLeft()
    turtle.select(5)
    local success, mess = turtle.suck()
    while success do
        item = turtle.getItemDetail()
        if item then
            if item.name == 'minecraft:sapling' then
                turtle.transferTo(1)
            elseif item.name == 'minecraft:dye' then
                turtle.transferTo(2)
            else
                turtle.dropDown()
            end
        end
        success, mess = turtle.suck()
    end
    turtle.turnRight()
    -- empty inventory
    for i=3,16 do
        turtle.select(i)
        turtle.dropDown()
    end
end

rednet.open("left")
rednet.host("tree", "planter1")

while 1 do
    print("Beginning cycle")
    print("Fuel level is at " .. turtle.getFuelLevel())
    refuel(25)
    check_bone_meal()
    emptyLeftoverChest()

    turtle.up()
    turtle.select(1)
    turtle.place()
    turtle.select(2)
    local item = turtle.getItemDetail()
    while item and item.count > 1 and turtle.place() do
        item = turtle.getItemDetail()
    end
    if item.count <= 1 then
        turtle.down()
        refill()
    else
        os.sleep(1)
        if dig() then
            turtle.forward()

            local success, blockUp = turtle.inspectUp()
            while not success or digUp() do
                turtle.up()
                success, blockUp = turtle.inspectUp()
            end
            while turtle.down() do
            end
            turtle.back()
        end
        turtle.down()
        while not waitForOtherRobot() do
        end
    end
end
