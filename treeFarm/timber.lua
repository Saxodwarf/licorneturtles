local allowed = {["minecraft:leaves2"] = true;
                 ["minecraft:log2"] = true}


function digUp()
    local item = turtle.inspectUp()
    if item and allowed[item.name] then
        turtle.digUp()
    else 
        return false
    end
    return true
end
function digDown()
    local item = turtle.inspectDown()
    if item and allowed[item.name] then
        turtle.digDown()
    else 
        return false
    end
    return true
end
function dig()
    local item = turtle.inspect()
    if item and allowed[item.name] then
        turtle.dig()
    else 
        return false
    end
    return true
end

function corner()
    digUp()
    digDown()
    turtle.turnLeft()
    dig()
    turtle.forward()
end

function side()
    digUp()
    digDown()
    dig()
    turtle.forward()
end

function digLeaveLayer()
    turtle.turnRight()
    for i=1, 4 do
        side()
        corner()
    end
    
    turtle.turnRight()
    turtle.dig()
    turtle.forward()
--     if turtle.detectUp() then
        turtle.turnLeft()
        for i=1, 2 do
            side()
            side()
            corner()
            side()
            side()
            side()
            corner()
            side()
        end
        turtle.turnLeft()
        turtle.forward()
--     else
--         turtle.back()
--         turtle.turnLeft()
--         turtle.turnLeft()
--     end
    return true
end

function empty()
    local item

    for i=1,16 do
        turtle.select(i)
        if turtle.getItemCount() > 0 then
            turtle.dropDown()
        end
    end
end

function tellOtherRobot(id)
    print("Telling robot " .. id)
    rednet.send(id, "done", "tree")
end

function refuel(limit)
    if turtle.getFuelLevel()< limit then
        turtle.back()
        turtle.turnRight()
        turtle.turnRight()
        
        turtle.select(16)
        turtle.suck()
        turtle.refuel()
        turtle.drop()
        turtle.turnRight()
        turtle.turnRight()
        turtle.forward()
    end
end

rednet.open("left")
local id
while id == nil do
    id = rednet.lookup("tree", "planter1")
end
local counter = 1
while 1 do
    counter = counter + 1
    local success, blockfront = turtle.inspect()
    if success and blockfront.name == "minecraft:log2" then
        counter = 1
        print("Beginning cycle")
        print("Fuel level is at " .. turtle.getFuelLevel())
        refuel(200)
        local success, blockUp = turtle.inspectUp()
        while not success or (digUp() and turtle.up() and digLeaveLayer())do
--             turtle.dig()
            turtle.up()
            success, blockUp = turtle.inspectUp()
        end

        while turtle.down() do
        end
        empty()
        tellOtherRobot(id)
    end
    os.sleep(0.8)
    if (counter %21) == 0 then
        -- Maybe the other robot did not get the message
        tellOtherRobot(id)
    end
end
