# LicorneTurtles

## Presentations

This project contains several computercraft programs, designed to make life around the LicorneCraft server easier.

## APIs

API references for CC Tweaked can be found [here](https://wiki.computercraft.cc/Main_Page).
API references for the original ComputerCraft can be found [here](http://computercraft.info/wiki/Category:APIs).