local args = { ... }
local username
if table.getn(args) < 1 then
    error("Pas de message à envoyer")
end
if table.getn(args) == 2 then
    username = args[2]
else
    username = "Le serveur Minecraft"
end
local headers = {
  [ "Content-Type" ] = "application/json",
}
local url = ""

local data = '{"username": "' .. username .. '",\
               "content": "' .. args[1].. '"}'
local handle, err, err_handle = http.post( url, data, headers)
if handle then 
    print("Message envoyé\nContenu: " .. args[1] .. "\nUsername: " .. username)
    handle.close()
else
    printError(err)
    if err_handle then 
        print(err_handle.readAll()) 
        err_handle.close()
    end
end
