-- Fills a rectangle with the block contained in the inventory
-- dimensions are passed as arguments :
-- fill <x> <y>
-- The turtle will begin by turning left.
local args = { ... }
if table.getn(args) < 2 then
    error("Pas de dimensions")
end
x = tonumber(args[1])
y = tonumber(args[2])

function place()
    cnt = 0
    while turtle.getItemCount() == 0 and cnt < 16 do
        turtle.select((turtle.getSelectedSlot())%16+1)
        cnt = cnt + 1
    end
    if cnt == 16 then
        return false
    else
        turtle.placeDown()
        return true
    end
end

function end_line(i, begin_right)
    begin_right = begin_right or 0
    -- if i is even, turn left, else turn right
    i = i + begin_right
    if i%2 == 0 then
        turtle.turnLeft()
    else
        turtle.turnRight()
    end
    if not place() then
        return false
    end
    turtle.forward()
    if i%2 == 0 then
        turtle.turnLeft()
    else
        turtle.turnRight()
    end
    return true
end

turtle.forward()
for i=1, x do
    for j=1,y-1 do
        if not place() then return end
        turtle.forward()
    end
    if i<x then
        if not end_line(i, 1) then return end
    end
end
place()
