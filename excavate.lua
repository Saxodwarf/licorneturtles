local width = 10
local depth = 5
for j=0, depth do
    turtle.dig()
    turtle.forward()
    if (j%2 == 0)
    then
        turtle.turnRight()
    else
        turtle.turnLeft()
    end
    for i=0, width do
        turtle.digUp()
        turtle.digDown()
        turtle.dig()
        turtle.forward()
    end
    if (j%2 == 0)
    then
        turtle.turnLeft()
    else
        turtle.turnRight()
    end
    turtle.digUp()
    turtle.digDown()
end
