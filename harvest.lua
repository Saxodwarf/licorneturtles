
function refill()
    local item
    for i in 2,16 do
        turtle.select(i)
        item = turtle.getItemDetail()
        if item then
            if item.name == 'minecraft:wheat_seeds' then
                turtle.transferTo(1)
            end
        end
    end
end

function plant()
    local item
    turtle.select(1)
    item = turtle.getItemDetail()
    if item then
        if item.name == 'minecraft:wheat_seeds' then
            if item.count < 2 then
                refill()
            end
        end
    end
    turtle.placeDown()
end

function end_line(i)
    -- if i is even, turn left, else turn right
    if i%2 == 0 then
        turtle.turnLeft()
    else
        turtle.turnRight()
    end
    turtle.digDown()
    plant()
    turtle.forward()
    if i%2 == 0 then
        turtle.turnLeft()
    else
        turtle.turnRight()
    end
end


function empty()
    local item

    for i=2,16 do
        turtle.select(i)
        item = turtle.getItemDetail()
        if item then
            if item.name == 'minecraft:wheat_seeds' then
                turtle.transferTo(1)
            end
            if turtle.getItemCount() > 0 then
                turtle.dropDown()
            end
        end
    end
end

turtle.forward()
for i=1, 7 do
    for j=1,6 do
    turtle.digDown()
    plant()
    turtle.forward()
    end
    if i<7 then
        end_line(i)
    end
end
turtle.digDown()
plant()
for j=1,2 do
    turtle.turnLeft()
    for i=1, 6 do
        turtle.forward()
    end
end
turtle.forward()
turtle.turnLeft()
turtle.turnLeft()
empty()
