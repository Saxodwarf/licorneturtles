function kill(laser)
    for i=0, 90, 5 do
        laser.fire(i, 0, 2.5)
    end
end


local TRESHOLD = 24
local modules = peripheral.wrap("back")
-- for _, entity in pairs(modules.sense()) do
--   print(("We found an entity (name: %s, uuid: %s)"):format(entity.name, entity.id))
-- end

while 1 do
    local are_all_adults = true
    local cow_counter = 0
    local entities = modules.sense()
    for i=1, #entities do
        local meta = modules.getMetaByID(entities[i].id)
        if meta then
        --       print(textutils.serialise(meta))
            z_range = meta.z <= 2 and meta.z >= 0
            x_range = meta.x <= 0 and meta.x >= 4
            if  z_range and x_range and meta.displayName == "Cow" then
                cow_counter = cow_counter + 1
                if meta.isChild then
                    are_all_adults = false
                    print("found a baby cow :(")
                end
            end
        end
    end
    if cow_counter > 0 then
        if are_all_adults then
            kill(modules.filterModules("plethora:laser"))
        end
    else
        -- admit cows
        rs.setOutput("right", true)
        os.sleep(0.5)
        rs.setOutput("right", false)
        os.pullEvent("redstone")
    end
    os.sleep(2)
end
